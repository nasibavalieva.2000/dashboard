import React ,{ useState, useEffect }from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import axios from 'axios';

import {
  ManagerCard,
  LatestSales,
  TotalProfit,
  TotalUsers,
} from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Dashboard = () =>   {
  const [component, setComponent] = useState('youtube');
  const [managers,setManagers] = useState([])
  const colors = ['#ED544F','#23A69A','#1976D4']

  // useEffect(() => {
    setInterval(() => {
      setComponent(component==='youtube' ? 'loshadki' : 'youtube');
    }, 300000);
    
  // }, []);
  useEffect(() => {
    const fetchData = async () => {
        const result = await axios(
          'http://161.35.84.159/',
        );
        // setIds(result.data.chatId);
        // var ids = []
        // result.data.forEach(data=>ids.push(data.chatId))
        setManagers(result.data.sort(compare).slice(0,5))
      };
      // setTop(managers.sort(compare).slice(0,3))
      fetchData();
    }, []);
  function compare(a, b) {
    const bandA = a.score;
    const bandB = b.score;

    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison* -1;
  }
  
  const classes = useStyles();
  return (
    <div 
    className={classes.root}
    >
      <Grid
        container
        spacing={3}
      >
        <Grid
          container
          spacing={3}
          item
          lg={6}
          md={12}
          xl={6}
          xs={12}
        >
          <Grid
            container
            item
            spacing={3}
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
          {
          colors.map(color => {
           return <Grid
              item
              lg={4}
              md={12}
              xl={4}
              xs={12}
            >
              <TotalProfit color ={color} ></TotalProfit>
            </Grid>
          })
          }
          </Grid>
          <Grid
            item
            spacing={1}
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
            <LatestSales 
            firstComponent={component}
            />
          </Grid>
          <Grid
            container
            item
            spacing={3}
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
          {
            colors.map(color => {
            return <Grid
              item
              lg={4}
              md={12}
              xl={4}
              xs={12}
            >
            <TotalUsers color ={color} ></TotalUsers>
            </Grid>
            })
          }
          </Grid>
        </Grid>
        <Grid
          container
          spacing= {3}
          item
          lg={6}
          md={12}
          xl={6}
          xs={12}
        >
          {
            managers.map(function(t,index) {
              return  <Grid
                      item
                      lg={6}
                      sm={6}
                      xl={6}
                      xs={12}
                    >
                    <ManagerCard 
                      cardId = {index}
                      deals ={t.score} 
                      // success={} 
                      // conversion={} 
                      name={t.name} 
                      // kpi={} 
                    />
                  </Grid>
            })
          }
      </Grid>
      </Grid>
    </div>
  );
};

export default Dashboard;
