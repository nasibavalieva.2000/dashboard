import React,{useState,useEffect} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { CardContent, Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  white: {
    color: theme.palette.white,
  },
  title:{
    color:theme.palette.warning.light
  }
}));
const Facts = props=> {
  const [facts,setFacts] = useState([])
  // const { className, ...rest } = props;
  
  // setInterval(() => {
  //     // var r = Math.floor((Math.random() * 100) + 1)
  //     // console.log(r+" "+facts)
  //   }, 10000);
  
  useEffect( () => {
      const fetchData = async () => {
          const result = await axios(
            'https://cat-fact.herokuapp.com/facts',
          );
          setFacts(result.data.all[Math.floor((Math.random() * 100) + 1)].text)
          // console.log(result.data.all[Math.floor((Math.random() * 100) + 1)].text)
        };
        fetchData();
    },[]);

  const classes = useStyles();
  return (
      <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
              <Typography className={classes.title} variant="h5">Факты про кошек </Typography>
            </Grid>
            <Grid item>
            <Typography className={classes.white} variant="body2">{facts}</Typography>
            </Grid>
          </Grid>
      </CardContent>
          
    );
  }

  Facts.propTypes = {
    className: PropTypes.string
  };
  export default Facts

 