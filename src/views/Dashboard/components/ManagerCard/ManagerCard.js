import React,{useState} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import FaceIcon from '@material-ui/icons/Face';
import ReactCardFlip from "react-card-flip";
import { Facts } from './components';


const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    backgroundColor:theme.palette.darkBlue
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    fontWeight: 700,
    color:theme.palette.warning.light,
    marginTop: '8px',
  },
  avatar: {
    backgroundColor: theme.palette.darkBlue,
    alignItems:'flex-start'
  },
  icon: {
    height: 32,
    width: 32
  },
  difference: {
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center'
  },
  differenceIcon: {
    color: theme.palette.warning.light
  },
  differenceValue: {
    color: theme.palette.warning.light,
    marginRight: theme.spacing(1)
  },
  white: {
    color: theme.palette.white,
  }
}));

const ManagerCard = props => {
  const { className, ...rest } = props;
  const [isFlipped, setIsFlipped] = useState(false);
  
  setInterval(() => {
    setIsFlipped(!isFlipped);
    // var r = Math.floor((Math.random() * 100) + 1)
    // console.log(r+" "+facts)
  }, 120000);

  const classes = useStyles();

  return (
    <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
      <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
        >
          <Grid item>
            <Typography
              className={classes.title}
              color="textSecondary"
              // gutterBottom
              variant="body2"
            >
              Менеджер: {props.name}
            </Typography>
            <Typography className={classes.white} variant="body1">Очки:{props.deals}</Typography>
            <Typography className={classes.white} variant="body1">Успешные:{props.cardId}</Typography>
            <Typography className={classes.white} variant="body1">Конверсия:</Typography>
            <Typography className={classes.white} variant="body1">KPI:</Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <FaceIcon className={classes.icon} />
            </Avatar>
          </Grid>
        </Grid>
        <div className={classes.difference}>
          <ArrowDownwardIcon className={classes.differenceIcon} />
          <Typography
            className={classes.differenceValue}
            variant="body2"
          >
            12%
          </Typography>
          <Typography
            className={classes.white}
            variant="caption"
          >
            Since last month
          </Typography>
        </div>
      </CardContent>
    </Card>
      
      <Card
      {...rest}
      className={clsx(classes.root, className)}
      >
       {(() =>{
        switch (props.cardId) {
          case 0:
            return <CardContent>
            <Grid
              container
              justify="space-between"
            >
              <Grid item>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  // gutterBottom
                  variant="body2"
                >
                  Менеджер: {props.name}
                </Typography>
                <Typography className={classes.white} variant="body1">Очки:{props.deals}</Typography>
                <Typography className={classes.white} variant="body1">Успешные:{props.cardId}</Typography>
                <Typography className={classes.white} variant="body1">Конверсия:</Typography>
                <Typography className={classes.white} variant="body1">KPI:</Typography>
              </Grid>
              <Grid item>
                <Avatar className={classes.avatar}>
                  <FaceIcon className={classes.icon} />
                </Avatar>
              </Grid>
            </Grid>
            <div className={classes.difference}>
              <ArrowDownwardIcon className={classes.differenceIcon} />
              <Typography
                className={classes.differenceValue}
                variant="body2"
              >
                12%
              </Typography>
              <Typography
                className={classes.white}
                variant="caption"
              >
                Since last month
              </Typography>
            </div>
          </CardContent>
          case 1:
            return <Facts></Facts>;
          case 2:
            return <CardContent>
            <Grid
              container
              justify="space-between"
            >
              <Grid item>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  // gutterBottom
                  variant="body2"
                >
                  Менеджер: {props.name}
                </Typography>
                <Typography className={classes.white} variant="body1">Очки:{props.deals}</Typography>
                <Typography className={classes.white} variant="body1">Успешные:{props.cardId}</Typography>
                <Typography className={classes.white} variant="body1">Конверсия:</Typography>
                <Typography className={classes.white} variant="body1">KPI:</Typography>
              </Grid>
              <Grid item>
                <Avatar className={classes.avatar}>
                  <FaceIcon className={classes.icon} />
                </Avatar>
              </Grid>
            </Grid>
            <div className={classes.difference}>
              <ArrowDownwardIcon className={classes.differenceIcon} />
              <Typography
                className={classes.differenceValue}
                variant="body2"
              >
                12%
              </Typography>
              <Typography
                className={classes.white}
                variant="caption"
              >
                Since last month
              </Typography>
            </div>
          </CardContent>
          case 3:
            return <Facts></Facts>
          case 4:
            return <CardContent>
            <Grid
              container
              justify="space-between"
            >
              <Grid item>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  // gutterBottom
                  variant="body2"
                >
                  Менеджер: {props.name}
                </Typography>
                <Typography className={classes.white} variant="body1">Очки:{props.deals}</Typography>
                <Typography className={classes.white} variant="body1">Успешные:{props.cardId}</Typography>
                <Typography className={classes.white} variant="body1">Конверсия:</Typography>
                <Typography className={classes.white} variant="body1">KPI:</Typography>
              </Grid>
              <Grid item>
                <Avatar className={classes.avatar}>
                  <FaceIcon className={classes.icon} />
                </Avatar>
              </Grid>
            </Grid>
            <div className={classes.difference}>
              <ArrowDownwardIcon className={classes.differenceIcon} />
              <Typography
                className={classes.differenceValue}
                variant="body2"
              >
                12%
              </Typography>
              <Typography
                className={classes.white}
                variant="caption"
              >
                Since last month
              </Typography>
            </div>
          </CardContent>
          default:
            return null;
        }
      })()}
    </Card>
    </ReactCardFlip>
  );
};

ManagerCard.propTypes = {
  className: PropTypes.string
};

export default ManagerCard;
