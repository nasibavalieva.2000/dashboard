import React from 'react';
import '/Applications/MAMP/htdocs/backend_projects/react-material-dashboard/src/assets/style.css'

function Row(props) {
    // let styleSheet = document.styleSheets[0]
    // console.log(styleSheet)
    // let animationName = `animation${props.name}`;
    // let keyframes =
    // `@-webkit-keyframes ${animationName} {
    //     0% {-webkit-transform:width:0px} 
    //     100% {-webkit-transform:width:${props.width}px}
    // }`;
    // styleSheet.insertRule(keyframes, styleSheet.cssRules.length);
    let style = {
        backgroundColor:props.color,
        width:props.width+'%',
        // animationName: animationName,
        animationTimingFunction: 'ease-out',
        animationDuration: '2s',
    };

    return (
    <li data-position={props.score}><span className="bar" style={style}></span><h3>{props.name}</h3></li>
    );
  }
  
  export default Row;
