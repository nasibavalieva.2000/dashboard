import React,{useState,useEffect} from 'react';
import Row from './Row';
import axios from 'axios';

function HorseList () {
  const [rows,setRows] = useState([]);
  const colors = ['#ED544F','#23A69A','#1976D4','#7E57C2','#F8B131']
  //comparison function for sort list
  function compare(a, b) {
    const bandA = a.score;
    const bandB = b.score;

    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison* -1;
  }

  //getting data 
  useEffect( () => {
      const fetchData = async () => {
          const result = await axios(
            'http://161.35.84.159/',
          );
          setRows(result.data.sort(compare));
        };
        fetchData();
    },[]);

  let style ={
      height:'380px'
  }
  return (
    <div style ={style}>
      <ul id="top" className="listitems">
          {
            rows.length ?
            rows.map((row,index) => <Row color = {colors[index]} width={row.score*100/30} key={row.name} score={row.score} name={row.name}></Row>) :
            null
          }
      </ul>
    </div>
  );
}


export default HorseList;