import React from 'react';
import ReactPlayer from "react-player"
import PropTypes from 'prop-types';
import { HorseList } from './components';

const LatestSales = props => {
  // const { className, ...rest } = props;
  switch(props.firstComponent){
    case 'youtube':
      return (
        <ReactPlayer 
          controls = {true}
          width = '626px'
          height = '380px'
          url="https://www.youtube.com/watch?v=i9E_Blai8vk"
          playing = {true}
        />
      );
    case 'loshadki':
      return (
          <HorseList></HorseList>
      )
    default:
      return null
  }
};

LatestSales.propTypes = {
  className: PropTypes.string
};

export default LatestSales;
