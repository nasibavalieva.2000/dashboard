import React,{useState,useEffect} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';

var weather = require('openweather-apis');
var ownKey='fca5ecfa117e5f887522a3dc92c5e276'

weather.setLang('ru');
weather.setCityId('1526384');
weather.setUnits('metric');
weather.setAPPID(ownKey);


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1)
  }
}));

const Footer = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const [info,setInfo] = useState('');

  useEffect(() => {
    const fetchData = async () => {
        weather.getSmartJSON( function(err, temp){
          const weatherString = 'Алматы,   Температура: ' +temp.temp+ " °C,   Влажность:" + temp.humidity + "%,"+ temp.description;
          setInfo(weatherString);
        })
      };
      fetchData();
  },[]);

  // var weatherString = '';
  // weather.getSmartJSON( function(err, temp){
  //   console.log(temp)
  //   weatherString += 'Алматы,   Температура: ' +temp.temp+ " °C,   Влажность:" + temp.humidity + "%,"+ temp.description;
  // });


  // const root = document.documentElement;
  // const marqueeElementsDisplayed = getComputedStyle(root).getPropertyValue("--marquee-elements-displayed");
  // const marqueeContent = document.querySelector("ul.marquee-content");
  // root.style.setProperty("--marquee-elements", marqueeContent.children.length);

  // for(let i=0; i<marqueeElementsDisplayed; i++) {
  //   marqueeContent.appendChild(marqueeContent.children[i].cloneNode(true));
  // }

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <div className="marquee">
        <ul className="marquee-content">
          <li></li>
          <li></li>
          <li></li>
          <li>{info}</li>
        </ul>
      </div>
    </div>
  );
};

Footer.propTypes = {
  className: PropTypes.string
};

export default Footer;
